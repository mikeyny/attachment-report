\contentsline {section}{\hbox to\@tempdima {\hfil } Abstract}{i}{section*.2}
\contentsline {section}{\hbox to\@tempdima {\hfil } Acknowledgement}{ii}{section*.3}
\contentsline {section}{\hbox to\@tempdima {\hfil } Acronyms and Abbreviations}{iii}{section*.4}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Organisational Background}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Organisational Structure}{1}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Organisational values}{2}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Organisational Vision}{3}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Organisational Motto}{3}{subsection.1.5}
\contentsline {section}{\numberline {2}Nature of Industry}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Products}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Services}{7}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Software Development}{7}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Transaction Switching}{7}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Resource Outsourcing}{8}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}Value Added Services (VAS)}{8}{subsubsection.2.2.4}
\contentsline {subsection}{\numberline {2.3}Key Departments in the SDLC}{9}{subsection.2.3}
\contentsline {section}{\numberline {3}Professional and Personal Development}{11}{section.3}
\contentsline {subsection}{\numberline {3.1}Professional Development}{11}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Android App Development}{11}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Web Development}{11}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}Version Control}{12}{subsubsection.3.1.3}
\contentsline {subsection}{\numberline {3.2}Personal Development}{13}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Team Work}{13}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Design}{13}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Presentation and Communication Skills}{13}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}Maintaining legacy systems}{14}{subsubsection.3.2.4}
\contentsline {subsubsection}{\numberline {3.2.5}Conflict Resolution between personal and organizational objectives}{14}{subsubsection.3.2.5}
\contentsline {subsection}{\numberline {3.3}Expectations and Industrial Reality}{14}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Expectations}{14}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Reality}{15}{subsubsection.3.3.2}
\contentsline {section}{\numberline {4}Projects UnderTaken}{16}{section.4}
\contentsline {subsection}{\numberline {4.1}Parks Mobile Receipting}{16}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Introduction}{16}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Description of Duties}{16}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Skills and Lessons Learnt}{18}{subsubsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.4}Project Images}{18}{subsubsection.4.1.4}
\contentsline {subsection}{\numberline {4.2}Afrosoft Website}{20}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Introduction}{20}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Description of Duties}{20}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}Skills and Lessons Learnt}{21}{subsubsection.4.2.3}
\contentsline {subsubsection}{\numberline {4.2.4}Project Images}{22}{subsubsection.4.2.4}
\contentsline {subsection}{\numberline {4.3}Instapay}{26}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Introduction}{26}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}Description of Duties}{26}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.3}Skills and Lessons Learnt}{28}{subsubsection.4.3.3}
\contentsline {subsubsection}{\numberline {4.3.4}Project Images}{28}{subsubsection.4.3.4}
\contentsline {subsection}{\numberline {4.4}Gitlab}{30}{subsection.4.4}
\contentsline {subsubsection}{\numberline {4.4.1}Introduction}{30}{subsubsection.4.4.1}
\contentsline {subsubsection}{\numberline {4.4.2}Description of Duties}{30}{subsubsection.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.3}Skills and Lessons Learnt}{31}{subsubsection.4.4.3}
\contentsline {subsection}{\numberline {4.5}Agropal}{32}{subsection.4.5}
\contentsline {subsubsection}{\numberline {4.5.1}Introduction}{32}{subsubsection.4.5.1}
\contentsline {subsubsection}{\numberline {4.5.2}Description of Duties}{32}{subsubsection.4.5.2}
\contentsline {subsubsection}{\numberline {4.5.3}Skills and Lessons Learnt}{33}{subsubsection.4.5.3}
\contentsline {subsubsection}{\numberline {4.5.4}Project Images}{34}{subsubsection.4.5.4}
\contentsline {subsection}{\numberline {4.6}Afropay Gateway}{36}{subsection.4.6}
\contentsline {subsubsection}{\numberline {4.6.1}Introduction}{36}{subsubsection.4.6.1}
\contentsline {subsubsection}{\numberline {4.6.2}Description of Duties}{36}{subsubsection.4.6.2}
\contentsline {subsubsection}{\numberline {4.6.3}Skills and Lessons Learnt}{36}{subsubsection.4.6.3}
\contentsline {subsubsection}{\numberline {4.6.4}Project Images}{38}{subsubsection.4.6.4}
\contentsline {section}{\numberline {5}Recommendations}{40}{section.5}
\contentsline {subsection}{\numberline {5.1}Challenges Faced}{40}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Recommendations to the Organisation}{41}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Recommendations to the University}{42}{subsection.5.3}
\contentsline {section}{\numberline {6}Conclusion}{43}{section.6}
